/*==============================================================*/
/* Nom de SGBD :  ORACLE Version 11g                            */
/* Date de cr�ation :  08/12/2022 08:42:50                      */
/*==============================================================*/


alter table ACHETER
   drop constraint FK_ACHETER_ACHETER_GROSSIST;

alter table ACHETER
   drop constraint FK_ACHETER_ACHETER2_FLEURS;

alter table COMMANDE
   drop constraint FK_COMMANDE_COMMANDER_CLIENT;

alter table COMPOSER_BOUQUET
   drop constraint FK_COMPOSER_COMPOSER__COMMANDE;

alter table COMPOSER_BOUQUET
   drop constraint FK_COMPOSER_COMPOSER__BOUQUET;

alter table COMPOSER_FLEUR
   drop constraint FK_COMPOSER_COMPOSER__BOUQUET2;

alter table COMPOSER_FLEUR
   drop constraint FK_COMPOSER_COMPOSER__FLEURS;

alter table HABITER
   drop constraint FK_HABITER_HABITER_VILLE;

alter table HABITER
   drop constraint FK_HABITER_HABITER2_CLIENT;

alter table LIVRER
   drop constraint FK_LIVRER_LIVRER_COMMANDE;

alter table LIVRER
   drop constraint FK_LIVRER_LIVRER2_VILLE;

drop index ACHETER2_FK;

drop index ACHETER_FK;

drop table ACHETER cascade constraints;

drop table BOUQUET cascade constraints;

drop table CLIENT cascade constraints;

drop index COMMANDER_FK;

drop table COMMANDE cascade constraints;

drop index COMPOSER_BOUQUET2_FK;

drop index COMPOSER_BOUQUET_FK;

drop table COMPOSER_BOUQUET cascade constraints;

drop index COMPOSER_FLEUR2_FK;

drop index COMPOSER_FLEUR_FK;

drop table COMPOSER_FLEUR cascade constraints;

drop table FLEURS cascade constraints;

drop table GROSSISTE cascade constraints;

drop index HABITER2_FK;

drop index HABITER_FK;

drop table HABITER cascade constraints;

drop index LIVRER2_FK;

drop index LIVRER_FK;

drop table LIVRER cascade constraints;

drop table VILLE cascade constraints;

/*==============================================================*/
/* Table : ACHETER                                              */
/*==============================================================*/
create table ACHETER 
(
   ID_GROSSISTE         SMALLINT             not null,
   ID_FLEUR             SMALLINT             not null,
   DATE_ACHAT           DATE                 not null,
   PRIX_ACHAT           FLOAT                not null,
   constraint PK_ACHETER primary key (ID_GROSSISTE, ID_FLEUR)
);

/*==============================================================*/
/* Index : ACHETER_FK                                           */
/*==============================================================*/
create index ACHETER_FK on ACHETER (
   ID_GROSSISTE ASC
);

/*==============================================================*/
/* Index : ACHETER2_FK                                          */
/*==============================================================*/
create index ACHETER2_FK on ACHETER (
   ID_FLEUR ASC
);

/*==============================================================*/
/* Table : BOUQUET                                              */
/*==============================================================*/
create table BOUQUET 
(
   ID_BOUQUET           SMALLINT             not null,
   PRIX_BOUQUET         FLOAT,
   constraint PK_BOUQUET primary key (ID_BOUQUET)
);

/*==============================================================*/
/* Table : CLIENT                                               */
/*==============================================================*/
create table CLIENT 
(
   IDCLIENT             SMALLINT             not null,
   NOM_CLIENT           CLOB,
   PRENOM_CLIENT        CLOB,
   ADRESSE_DE_FACTURATION CLOB,
   EMAIL                CLOB,
   TELEPHONE            INTEGER,
   constraint PK_CLIENT primary key (IDCLIENT)
);

/*==============================================================*/
/* Table : COMMANDE                                             */
/*==============================================================*/
create table COMMANDE 
(
   IDCOMMANDE           SMALLINT             not null,
   IDCLIENT             SMALLINT             not null,
   NOM_DESTINATAIRE     CLOB,
   ADRESSE_LIVRAISON    CLOB,
   DATE_LIVRAISON_PREVUE DATE,
   DATE_LIVRAISON_EFFECTUEE DATE,
   PRIX_TTC             FLOAT,
   DATE_CONFIRMATION    DATE,
   DATE_FACTURE         DATE,
   DATE_REGLEMENT       DATE,
   constraint PK_COMMANDE primary key (IDCOMMANDE)
);

/*==============================================================*/
/* Index : COMMANDER_FK                                         */
/*==============================================================*/
create index COMMANDER_FK on COMMANDE (
   IDCLIENT ASC
);

/*==============================================================*/
/* Table : COMPOSER_BOUQUET                                     */
/*==============================================================*/
create table COMPOSER_BOUQUET 
(
   IDCOMMANDE           SMALLINT             not null,
   ID_BOUQUET           SMALLINT             not null,
   NB_BOUQET            INTEGER              not null,
   constraint PK_COMPOSER_BOUQUET primary key (IDCOMMANDE, ID_BOUQUET)
);

/*==============================================================*/
/* Index : COMPOSER_BOUQUET_FK                                  */
/*==============================================================*/
create index COMPOSER_BOUQUET_FK on COMPOSER_BOUQUET (
   IDCOMMANDE ASC
);

/*==============================================================*/
/* Index : COMPOSER_BOUQUET2_FK                                 */
/*==============================================================*/
create index COMPOSER_BOUQUET2_FK on COMPOSER_BOUQUET (
   ID_BOUQUET ASC
);

/*==============================================================*/
/* Table : COMPOSER_FLEUR                                       */
/*==============================================================*/
create table COMPOSER_FLEUR 
(
   ID_BOUQUET           SMALLINT             not null,
   ID_FLEUR             SMALLINT             not null,
   NB_FLEURS            INTEGER              not null,
   constraint PK_COMPOSER_FLEUR primary key (ID_BOUQUET, ID_FLEUR)
);

/*==============================================================*/
/* Index : COMPOSER_FLEUR_FK                                    */
/*==============================================================*/
create index COMPOSER_FLEUR_FK on COMPOSER_FLEUR (
   ID_BOUQUET ASC
);

/*==============================================================*/
/* Index : COMPOSER_FLEUR2_FK                                   */
/*==============================================================*/
create index COMPOSER_FLEUR2_FK on COMPOSER_FLEUR (
   ID_FLEUR ASC
);

/*==============================================================*/
/* Table : FLEURS                                               */
/*==============================================================*/
create table FLEURS 
(
   ID_FLEUR             SMALLINT             not null,
   NOM_FLEUR            CLOB,
   PRIX_FLEUR           FLOAT,
   constraint PK_FLEURS primary key (ID_FLEUR)
);

/*==============================================================*/
/* Table : GROSSISTE                                            */
/*==============================================================*/
create table GROSSISTE 
(
   ID_GROSSISTE         SMALLINT             not null,
   NOM_GROSSISTE        CLOB,
   constraint PK_GROSSISTE primary key (ID_GROSSISTE)
);

/*==============================================================*/
/* Table : HABITER                                              */
/*==============================================================*/
create table HABITER 
(
   ID_VILLE             SMALLINT             not null,
   IDCLIENT             SMALLINT             not null,
   constraint PK_HABITER primary key (ID_VILLE, IDCLIENT)
);

/*==============================================================*/
/* Index : HABITER_FK                                           */
/*==============================================================*/
create index HABITER_FK on HABITER (
   ID_VILLE ASC
);

/*==============================================================*/
/* Index : HABITER2_FK                                          */
/*==============================================================*/
create index HABITER2_FK on HABITER (
   IDCLIENT ASC
);

/*==============================================================*/
/* Table : LIVRER                                               */
/*==============================================================*/
create table LIVRER 
(
   IDCOMMANDE           SMALLINT             not null,
   ID_VILLE             SMALLINT             not null,
   constraint PK_LIVRER primary key (IDCOMMANDE, ID_VILLE)
);

/*==============================================================*/
/* Index : LIVRER_FK                                            */
/*==============================================================*/
create index LIVRER_FK on LIVRER (
   IDCOMMANDE ASC
);

/*==============================================================*/
/* Index : LIVRER2_FK                                           */
/*==============================================================*/
create index LIVRER2_FK on LIVRER (
   ID_VILLE ASC
);

/*==============================================================*/
/* Table : VILLE                                                */
/*==============================================================*/
create table VILLE 
(
   ID_VILLE             SMALLINT             not null,
   NOM_VILLE            CLOB,
   CODE_POSTAL          INTEGER,
   TARIF_LIVRAISON      FLOAT,
   constraint PK_VILLE primary key (ID_VILLE)
);

alter table ACHETER
   add constraint FK_ACHETER_ACHETER_GROSSIST foreign key (ID_GROSSISTE)
      references GROSSISTE (ID_GROSSISTE);

alter table ACHETER
   add constraint FK_ACHETER_ACHETER2_FLEURS foreign key (ID_FLEUR)
      references FLEURS (ID_FLEUR);

alter table COMMANDE
   add constraint FK_COMMANDE_COMMANDER_CLIENT foreign key (IDCLIENT)
      references CLIENT (IDCLIENT);

alter table COMPOSER_BOUQUET
   add constraint FK_COMPOSER_COMPOSER__COMMANDE foreign key (IDCOMMANDE)
      references COMMANDE (IDCOMMANDE);

alter table COMPOSER_BOUQUET
   add constraint FK_COMPOSER_COMPOSER__BOUQUET foreign key (ID_BOUQUET)
      references BOUQUET (ID_BOUQUET);

alter table COMPOSER_FLEUR
   add constraint FK_COMPOSER_COMPOSER__BOUQUET2 foreign key (ID_BOUQUET)
      references BOUQUET (ID_BOUQUET);

alter table COMPOSER_FLEUR
   add constraint FK_COMPOSER_COMPOSER__FLEURS foreign key (ID_FLEUR)
      references FLEURS (ID_FLEUR);

alter table HABITER
   add constraint FK_HABITER_HABITER_VILLE foreign key (ID_VILLE)
      references VILLE (ID_VILLE);

alter table HABITER
   add constraint FK_HABITER_HABITER2_CLIENT foreign key (IDCLIENT)
      references CLIENT (IDCLIENT);

alter table LIVRER
   add constraint FK_LIVRER_LIVRER_COMMANDE foreign key (IDCOMMANDE)
      references COMMANDE (IDCOMMANDE);

alter table LIVRER
   add constraint FK_LIVRER_LIVRER2_VILLE foreign key (ID_VILLE)
      references VILLE (ID_VILLE);


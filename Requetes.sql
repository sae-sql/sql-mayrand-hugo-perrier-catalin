--R1
select count(idcommande) from commande 
where extract(year from date_reglement)='2021';
--R2
select code_postal,count(idcommande) ncommande
from livrer l 
join (select id_ville,code_postal from ville) v on l.id_ville=v.id_ville group by code_postal order by ncommande desc fetch first 3 row only;
--R3
select id_fleur,sum(mfleur) nb2021 from (select idcommande 
    from commande 
    where extract(year from date_reglement)='2021') c
join (select idcommande,id_fleur,nb_bouquet*nb_fleurs mfleur
    from composer_fleur cf 
    join composer_bouquet cb 
    on cf.id_bouquet=cb.id_bouquet 
    order by idcommande) f 
on f.idcommande=c.idcommande group by id_fleur order by nb2021 desc fetch first 1 row only;
--R4
select idclient,coalesce(avg(sommebouquet),0) moyenne_bouquet from commande c left join (select idcommande,sum(nb_bouquet)sommebouquet 
    from composer_bouquet group by idcommande) nb
on c.idcommande=nb.idcommande group by idclient order by idclient;
--R5
select id_fleur from fleurs minus (select distinct id_fleur from composer_fleur);
--R6
select idcommande,id_fleur,nb_bouquet,nb_fleurs 
from composer_fleur cf 
join composer_bouquet cb 
on cf.id_bouquet=cb.id_bouquet order by idcommande;



select cl.idclient,sum(mfleur) nbfleurclient,id_fleur
from client cl 
join commande c
on cl.idclient=c.idclient
left join (select idcommande,id_fleur,nb_bouquet*nb_fleurs mfleur
    from composer_fleur cf 
    join composer_bouquet cb 
    on cf.id_bouquet=cb.id_bouquet order by idcommande) f
on c.idcommande=f.idcommande 
group by cl.idclient,id_fleur
order by cl.idclient;
--R7
select idcommande numero_commande,date_commande,nom_client,prix_ttc from commande join client using(idclient);
--R8
select nom_client from client 
where idclient=(select idclient 
from commande 
where prix_ttc=(select max(prix_ttc) from commande));
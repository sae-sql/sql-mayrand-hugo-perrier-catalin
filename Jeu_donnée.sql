/*Ville---------------------------------------*/
insert into ville values(1,'Lyon',69000,5);
insert into ville values(2,'Saint-Genis-Laval',69230,17);
insert into ville values(3,'Villeurbanne',69100,8);
insert into ville values(4,'Ecully',69130,8);
insert into ville values(5,'Caluire-et-Cuire',69034,10);
insert into ville values(6,'Bron',69500,10);
insert into ville values(7,'Oullins',69600,15);
insert into ville values(8,'Saint-Fons',69199,12);
insert into ville values(9,'Vaulx-en-Velin',69120,8);
insert into ville values(10,'Rillieux-la-Pape',69286,17);
/*---------------------------------------------*/

/*Client------------------------------------------*/
insert into CLIENT values(1,'D�biann','Pierre','36 Chemin du Moulin Caron','pierre.debiann@mail.fr','0524658935');
insert into CLIENT values(2,'Mac Intoche','Julie','15 Route d Irigny','macintochejulie@mail.fr','0685653246');
insert into CLIENT values(3,'Commodort','Camille','8 Rue Guillermin','camillecommodort13@mail.fr','0545251434');
insert into CLIENT values(4,'Pascale','G�rard','23 Rue des Menestrels','gege.pascale@mail.fr','0785214212');
insert into CLIENT values(5,'Rubi','Mehdi','55 Rue Emile Zola','rubimehdi@mail.fr','0421364222');
insert into CLIENT values(6,'Swift','Sarah','12 Chemin du Rafour','sarah.swift@mail.fr','0754236106');
insert into CLIENT values(7,'Philippe','Jimmy','14 Avenue de l Europe','jimmy.philippe84@mail.fr','0684963296');
insert into CLIENT values(8,'Havira','Jeanne','73 Cours Emile Zola','jeannehavira@mail.fr','0741213474');
/*---------------------------------------------*/

/*Habiter---------------------------------------*/
insert into habiter values(4,1);
insert into habiter values(2,2);
insert into habiter values(6,3);
insert into habiter values(2,4);
insert into habiter values(9,5);
insert into habiter values(4,6);
insert into habiter values(10,7);
insert into habiter values(3,8);
/*---------------------------------------------*/

/*Grossiste---------------------------------------*/
insert into grossiste values(1,'Sud Fleurs');
insert into grossiste values(2,'Fleurs de France');
insert into grossiste values(3,'Roses de Provence');
/*---------------------------------------------*/

/*Fleurs---------------------------------------*/
insert into fleurs values(1,'Rose',1);
insert into fleurs values(2,'Tulipe rouge',0.5);
insert into fleurs values(3,'Tulipe jaune',0.5);
insert into fleurs values(4,'Muguet',0.9);
insert into fleurs values(5,'Pivoine',0.6);
insert into fleurs values(6,'P�tunia',0.7);
/*---------------------------------------------*/

/*Bouquets---------------------------------------*/
insert into bouquet values(1,20);
insert into bouquet values(2,13);
insert into bouquet values(3,11.5);
insert into bouquet values(4,15);
/*---------------------------------------------*/

/*Composer fleurs---------------------------------------*/
insert into composer_fleur values(1,4,20);
insert into composer_fleur values(2,1,3);
insert into composer_fleur values(2,5,5);
insert into composer_fleur values(2,3,10);
insert into composer_fleur values(3,3,10);
insert into composer_fleur values(3,2,10);
insert into composer_fleur values(4,5,4);
insert into composer_fleur values(4,4,12);
/*---------------------------------------------*/

/*Acheter---------------------------------------*/
insert into acheter values(1,2,'03/01/2023',0.3,150);
insert into acheter values(3,1,'28/11/2022',0.7,200);
insert into acheter values(1,3,'05/12/2022',0.3,120);
insert into acheter values(2,5,'20/12/2022',0.4,80);
insert into acheter values(1,4,'03/01/2023',0.6,300);
/*---------------------------------------------*/

/*Commande---------------------------------------*/
insert into commande values(1,6,'Dupond','20 Rue de Valence','04/01/2023','05/01/2023',46.2,'27/12/2022','05/01/2023','28/12/2022','27/12/2022');
insert into commande values(2,4,'Lagaffe','8 Rue du Buisset','29/11/2022','29/11/2022',33.6,'20/11/2022','29/11/2022','22/11/2022','19/11/2022');
insert into commande values(3,1,'Gouache','5 Chemin de Combe-Martin','07/12/2022','07/12/2022',43.2,'28/11/2022','07/12/2022','01/12/2022','25/11/2022');
insert into commande values(4,8,'Juste','14 Avenue de Gadagne','20/12/2022','20/12/2022',74.4,'10/11/2022','20/12/2022','10/11/2022','10/11/2022');
insert into commande(idcommande,idclient,nom_destinataire,adresse_livraison,date_commande) values(5,2,'Chapelle','3 Avenue Victor Hugo','05/07/2021');
insert into commande(idcommande,idclient,nom_destinataire,adresse_livraison,date_livraison_prevue,prix_ttc,date_confirmation,date_commande) values(6,7,'Marie','48 Avenue Camille Rousset','05/01/2023',108,'15/12/2022','15/12/2022');
insert into commande values(7,5,'Clouzot','74 Place Jules Grandcl�ment','25/12/2022','25/12/2022',39,'10/12/2022','25/12/2022','12/12/2022','09/12/2022');
insert into commande(idcommande,idclient,nom_destinataire,adresse_livraison,date_livraison_prevue,prix_ttc,date_confirmation,date_reglement,date_commande) values(8,1,'Morse','127 bis Avenue Lacassagne','05/02/2023',19.8,'02/01/2023','03/01/2023','31/12/2022');
insert into commande values(9,7,'Wayne','18 Rue Paul Eluard','06/12/2022','06/12/2022',57.6,'04/12/2022','06/12/2022','04/12/2022','25/11/2022');
insert into commande values(10,3,'Barnab�','6 Chemin de Charri�re Blanche','01/12/2022','01/12/2022',37.2,'27/11/2022','01/12/2022','28/11/2022','27/11/2022');
insert into commande values(11,1,'Verlaine','50 Rue Jean-Baptiste Cl�ment','28/07/2021','28/07/2021',207,'10/06/2021','28/07/2021','20/06/2021','05/06/2021');
/*---------------------------------------------*/

/*Livrer---------------------------------------*/
insert into livrer values(1,8);
insert into livrer values(2,7);
insert into livrer values(3,5);
insert into livrer values(4,2);
insert into livrer values(5,10);
insert into livrer values(6,6);
insert into livrer values(7,3);
insert into livrer values(8,1);
insert into livrer values(9,9);
insert into livrer values(10,4);
insert into livrer values(11,3);
/*---------------------------------------------*/

/*Composer bouquets---------------------------------------*/
insert into composer_bouquet values(1,4,1);
insert into composer_bouquet values(1,3,1);
insert into composer_bouquet values(2,2,1);
insert into composer_bouquet values(3,2,2);
insert into composer_bouquet values(4,4,3);
insert into composer_bouquet values(6,3,1);
insert into composer_bouquet values(7,2,1);
insert into composer_bouquet values(7,1,4);
insert into composer_bouquet values(8,3,1);
insert into composer_bouquet values(9,2,3);
insert into composer_bouquet values(10,3,2);
insert into composer_bouquet values(11,3,5);
insert into composer_bouquet values(11,1,6);
/*---------------------------------------------*/